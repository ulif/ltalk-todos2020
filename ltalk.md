
# 2020 und Datenschutz - was steht an?

## EU - Probleme
  * *Digital Services Act*

      Dienstleisterrichtlinie

      Themen u.a.: Urheberrecht

      Gefahr: Uploadfilter

  * *e-Evidence*

      Elektronische Beweisemittel

      Gefahr: Übergehen von Landesrecht

  * *e-Privacy*

      Verbraucher.innen-Rechte vor Tracking

      Zustand: keine Vitalfunktionen

      Gefahr: wurde umgedreht (Rechte der Unternehmen auf Daten), Uploadfilter


## EU - noch mehr Probleme

  * *Terrorrichtlinie*

  * *Passenger Name Records*

      Gefahr: Ausweitung auf Zug, Schiff, Bus, Bahn, .....


  * *Gesichtserkennung* (Verbot? Videoüberwachungskonsultationen)

  * *Vorratsdatenspeicherung*

  * *Privacy Shield*

  * *7-Jahresplan*

      Gefahr: viele Überwachungsprojekte

      Zusammenführen von Datenbanken

      Überwachung Außengrenzen


## EU - Problemlösungen

  * Frühjahr
    * Urteil wg. Passenger-Name-Records
    * Urteil wg. VDS in Belgien (und UK?)

  * November 2020
    * *Freedom not Fear 2020*
        https://freedomnotfear.org
        Jubiläum: 10. Geburtstag \\o/
        Vorbereitung bereits ab April

## BRD - Probleme

   * *Gesichtserkennung*

   * *Bundespolizeigesetz*

     * Erweiterungen der ganzen Länderpolizeigesetze

   * *Rise of Faschismus*

   * *Bundestrojaner*

   * *Vorratsdatenspeicherung*

   * *Cyberwar*-Orientierung auf Angriff

   * *Bestandsdatenabfrage* (Passwortherausgabe)

   * *Ausweitung des Vorfeld des Vorfelds* (Gefährder.innen)

   * *Hatespeech*

   * *Zensurfilter*


## BRD - Entscheidungen

   * *Urteil wg. VDS*

   * *Urteil wg. Bestandsdatenspeicherung*


## BRD - Lösungen

   * *Erster Mai*

   * *Lesung gegen Überwachung (6.6.)*

   * *BBAs* (Streaming im Space)

   * *Workshops für die VZ Niedersachsen (16.6)*

   * *37C3*

   * *Privacy Salons*


## Download

  Slides: https://codeberg.org/ulif/ltalk-todos2020
